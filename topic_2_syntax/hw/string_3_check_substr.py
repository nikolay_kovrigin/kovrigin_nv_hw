def check_substr(s1, s2):
    """
    Функция check_substr.

    Принимает две строки.
    Если меньшая по длине строка содержится в большей, то возвращает True,
    иначе False.
    Если строки равны, то False.
    Если одна из строк пустая, то True.
    """
    if (not s1 and s2) or (not s2 and s1):
        return True
    if s1 == s2:
        return False
    if len(s1) < len(s2):
        return s2.find(s1) > -1
    if len(s2) < len(s1):
        return s1.find(s2) > -1
    return False


if __name__ == '__main__':
    print(check_substr('qwe','qe'))