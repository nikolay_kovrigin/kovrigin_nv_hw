def print_hi(n):
    """
    Функция print_hi.

    Принимает число n.
    Выведите на экран n раз фразу "Hi, friend!"
    Если число <= 0, тогда возвращается пустая строка.
    Пример: n=3, тогда в результате "Hi, friend!Hi, friend!Hi, friend!"
    """

    print(n * "Hi, friend!")

    # # альтернатива через цикл
    #
    # if n > 0:
    #     i = 0
    #     hi = str()
    #     while i < n:
    #         hi += "Hi, friend!"
    #         i += 1
    #     print(hi)
    # else:
    #     print('')


if __name__ == '__main__':
    print_hi(-1)
    print_hi(0)
    print_hi(-2)
