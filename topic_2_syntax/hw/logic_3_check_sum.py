def check_sum(num1, num2, num3):
    """
    Функция check_sum.

    Принимает 3 числа.
    Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
    """
    return (num1 + num2 == num3) or (num1 + num3 == num2) or (num2 + num3 == num1)
