def print_nth_symbols(s, n):
    """
    Функция print_nth_symbols.

    Принимает строку и натуральное число n (целое число > 0).
    Вывести символы с индексом n, n*2, n*3 и так далее.
    Пример: string='123456789qwertyuiop', n = 2 => result='3579wryip'

    Если число меньше или равно 0, то вывести строку 'Must be > 0!'.
    Если тип n не int, то вывести строку 'Must be int!'.

    Если n больше длины строки, то вывести пустую строку.
    """

    if type(n) != int:
        print('Must be int!')
    elif n <= 0:
        print('Must be > 0!')
    elif n > len(s):
        print('')
    else:
        i = 1
        acc = ""
        while i * n < len(s):
            acc += s[i * n]
            i += 1
        print(acc)


if __name__ == '__main__':
    print_nth_symbols('123456789', 2)
