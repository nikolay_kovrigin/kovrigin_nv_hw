from project_students.combat.character import Character
from project_students.combat.character_body_points import BodyPoints
from project_students.combat.character_states import CharacterState
from project_students.combat.character_types import CharacterType


class TestCharacterClass:
    name = 'Test'
    c_type = CharacterType.USUAL
    hp = 100
    hit = 100
    chance_1 = None
    chance_2 = 50

    def test_init(self):
        test_character = Character(name=self.__class__.name,
                                   character_type=self.__class__.c_type,
                                   hp=self.__class__.hp,
                                   hit=self.__class__.hit,
                                   hit_chance=self.__class__.chance_1)
        assert test_character.name == self.__class__.name
        assert test_character.character_type == self.__class__.c_type
        assert test_character.hp == self.__class__.hp
        assert test_character.hit_power == self.__class__.hit
        assert test_character.hit_chance == self.__class__.chance_1
        assert test_character.state == CharacterState.READY

    def test_str(self):
        test_character_1 = Character(name=self.__class__.name,
                                     character_type=self.__class__.c_type,
                                     hp=self.__class__.hp,
                                     hit=self.__class__.hit,
                                     hit_chance=self.__class__.chance_1)
        assert str(test_character_1) == f'Name: {self.__class__.name}\n' \
                                        f'Type: {self.__class__.c_type.name}\n' \
                                        f'HP: {self.__class__.hp} | HIT: {self.__class__.hit}'

        test_character_2 = Character(name=self.__class__.name,
                                     character_type=self.__class__.c_type,
                                     hp=self.__class__.hp,
                                     hit=self.__class__.hit,
                                     hit_chance=self.__class__.chance_2)
        assert str(test_character_2) == f'Name: {self.__class__.name}\n' \
                                        f'Type: {self.__class__.c_type.name}\n' \
                                        f'HP: {self.__class__.hp} | HIT: {self.__class__.hit} | ' \
                                        f'CHANCE: {self.__class__.chance_2}%'

    def test_next_step(self):
        test_character = Character(name=self.__class__.name,
                                   character_type=self.__class__.c_type,
                                   hp=self.__class__.hp,
                                   hit=self.__class__.hit,
                                   hit_chance=self.__class__.chance_1)
        test_character.next_step_points(BodyPoints.NOTHING, BodyPoints.HEAD)
        assert test_character.attack_point == BodyPoints.NOTHING
        assert test_character.defence_point == BodyPoints.HEAD

        test_character.next_step_points(BodyPoints.BELLY, BodyPoints.LEGS)
        assert test_character.attack_point == BodyPoints.BELLY
        assert test_character.defence_point == BodyPoints.LEGS

    def test_get_hit(self):
        test_character_1 = Character(name=self.__class__.name,
                                     character_type=self.__class__.c_type,
                                     hp=self.__class__.hp,
                                     hit=self.__class__.hit,
                                     hit_chance=self.__class__.chance_1)
        test_character_1.next_step_points(BodyPoints.NOTHING, BodyPoints.LEGS)
        assert test_character_1.get_hit(BodyPoints.LEGS, 50, CharacterType.MENTAL) == "Hit is blocked!"

        test_character_1.next_step_points(BodyPoints.NOTHING, BodyPoints.NOTHING)
        assert test_character_1.get_hit(BodyPoints.NOTHING, 50, CharacterType.WEAPON) == "Opponent missed!"

        assert test_character_1.get_hit(BodyPoints.HEAD, 50, CharacterType.MENTAL) == "I'm damaged"
        assert test_character_1.hp == 50

        assert test_character_1.get_hit(BodyPoints.HEAD, 50, CharacterType.USUAL) == "I've lost..."
        assert test_character_1.hp == 0
        assert test_character_1.state == CharacterState.DEFEATED
