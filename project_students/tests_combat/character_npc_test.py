import random

from project_students.combat.character_body_points import BodyPoints
from project_students.combat.character_npc import CharacterNPC
from project_students.combat.character_states import CharacterState
from project_students.combat.character_types import CharacterType


class TestCharacterNPCClass:
    name = 'Usual Goblin'
    c_type = CharacterType.USUAL
    hp = 10
    hit = 2
    chance = 100

    @staticmethod
    def setup_method():
        random.seed(42)

    def test_init_npc(self):
        test_character = CharacterNPC()
        assert test_character.name == self.__class__.name
        assert test_character.character_type == self.__class__.c_type
        assert test_character.hp == self.__class__.hp
        assert test_character.hit_power == self.__class__.hit
        assert test_character.hit_chance == self.__class__.chance
        assert test_character.state == CharacterState.READY

    def test_str_npc(self):
        test_character = CharacterNPC()
        assert str(test_character) == f'Name: {self.__class__.name}\n' \
                                      f'Type: {self.__class__.c_type.name}\n' \
                                      f'HP: {self.__class__.hp} | HIT: {self.__class__.hit} | ' \
                                      f'CHANCE: {self.__class__.chance}%'

    def test_next_step(self):
        test_character = CharacterNPC()

        test_character.next_step_points()
        assert test_character.attack_point == BodyPoints.LEGS
        assert test_character.defence_point == BodyPoints.LEGS

        test_character.next_step_points()
        assert test_character.attack_point == BodyPoints.HEAD
        assert test_character.defence_point == BodyPoints.BELLY
