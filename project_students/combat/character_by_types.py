from project_students.combat.character_types import CharacterType

character_by_type = {
    CharacterType.USUAL: {'Usual Goblin': 'usual_goblin',
                          'Usual Orc': 'usual_orc',
                          'Usual Troll': 'usual_troll'},

    CharacterType.PHYSIC: {'Physic Goblin': 'physic_goblin',
                             'Physic Orc': 'physic_orc',
                             'Physic Troll': 'physic_troll'},

    CharacterType.MENTAL: {'Mental Goblin': 'mental_goblin',
                           'Mental Orc': 'mental_orc',
                           'Mental Troll': 'mental_troll'},

    CharacterType.HITECH: {'HiTech Goblin': 'hitech_goblin',
                           'HiTech Orc': 'hitech_orc',
                           'HiTech Troll': 'hitech_troll'},

    CharacterType.WEAPON: {'Weapon Goblin': 'weapon_goblin',
                           'Weapon Orc': 'weapon_orc',
                           'Weapon Troll': 'weapon_troll'}
}

character_characteristic = {
    'Goblin': {'hp': 10, 'hit': 2, 'hit_chance': 100},
    'Orc': {'hp': 20, 'hit': 4, 'hit_chance': 70},
    'Troll': {'hp': 30, 'hit': 10, 'hit_chance': 30}
}
