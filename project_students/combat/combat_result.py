from enum import Enum, auto


class CombatResult(Enum):
    WIN = auto()
    LOSE = auto()
