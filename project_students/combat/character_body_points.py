from enum import Enum, auto


class BodyPoints(Enum):
    HEAD = auto()
    BELLY = auto()
    LEGS = auto()
    NOTHING = auto()

    @classmethod
    def min_value(cls):
        return cls.HEAD.value

    @classmethod
    def max_value(cls):
        return cls.NOTHING.value

    @classmethod
    def has_item(cls, name: str):
        return name in cls._member_names_
