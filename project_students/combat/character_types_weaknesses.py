from project_students.combat.character_types import CharacterType

character_weaknesses_by_type = {

    CharacterType.USUAL: (CharacterType.USUAL,),

    CharacterType.PHYSIC: (CharacterType.HITECH,
                           CharacterType.WEAPON),

    CharacterType.MENTAL: (CharacterType.PHYSIC,
                           CharacterType.WEAPON),

    CharacterType.HITECH: (CharacterType.MENTAL,
                           CharacterType.PHYSIC),

    CharacterType.WEAPON: (CharacterType.MENTAL,
                           CharacterType.HITECH)
}
