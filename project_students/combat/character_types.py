from enum import Enum, auto


class CharacterType(Enum):
    USUAL = auto()
    PHYSIC = auto()
    MENTAL = auto()
    HITECH = auto()
    WEAPON = auto()

    @classmethod
    def min_value(cls):
        return cls.USUAL.value

    @classmethod
    def max_value(cls):
        return cls.WEAPON.value
