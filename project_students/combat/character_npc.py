import random

from project_students.combat.character import Character
from project_students.combat.character_body_points import BodyPoints
from project_students.combat.character_by_types import character_by_type, character_characteristic
from project_students.combat.character_types import CharacterType


class CharacterNPC(Character):
    def __init__(self):
        rand_type_value = random.randint(CharacterType.min_value(), CharacterType.max_value())
        rand_character_type = CharacterType(rand_type_value)
        rand_character_name = random.choice(list(character_by_type.get(rand_character_type, {}).keys()))
        hp, hit, hit_chance = (character_characteristic[rand_character_name.split(' ')[1]].values())
        if hit_chance > 100:
            hit_chance = 100
        elif hit_chance <= 0:
            hit_chance = 1
        super().__init__(rand_character_name, rand_character_type, hp, hit, hit_chance)

    def next_step_points(self, **kwargs):
        attack_point = BodyPoints(random.choice(list(range(BodyPoints.min_value(), BodyPoints.max_value())) +
                                          [BodyPoints.NOTHING.value] * int(abs(self.hit_chance - 100)/10)))
        defence_point = BodyPoints(random.randint(BodyPoints.min_value(), BodyPoints.max_value()))
        super().next_step_points(next_attack=attack_point, next_defence=defence_point)


if __name__ == '__main__':
    npc = CharacterNPC()
    print(npc)
