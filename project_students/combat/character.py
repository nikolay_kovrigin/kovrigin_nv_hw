from project_students.combat.character_body_points import BodyPoints
from project_students.combat.character_states import CharacterState
from project_students.combat.character_types import CharacterType
from project_students.combat.character_types_weaknesses import character_weaknesses_by_type as weaknesses_by_type


class Character:
    def __init__(self,
                 name: str,
                 character_type: CharacterType,
                 hp=50,
                 hit=5,
                 hit_chance=None):
        self.name = name
        self.character_type = character_type
        self.weaknesses = weaknesses_by_type.get(character_type, tuple())
        self.attack_point = None
        self.defence_point = None
        self.hp = hp
        self.hit_power = hit
        self.hit_chance = hit_chance
        self.state = CharacterState.READY

    def __str__(self):
        return \
            f'Name: {self.name}\nType: {self.character_type.name}\n' \
            f'HP: {self.hp} | HIT: {self.hit_power} | CHANCE: {self.hit_chance}%' \
            if self.hit_chance \
            else f'Name: {self.name}\nType: {self.character_type.name}\n' \
                 f'HP: {self.hp} | HIT: {self.hit_power}'

    def next_step_points(self,
                         next_attack: BodyPoints,
                         next_defence: BodyPoints):
        self.attack_point = next_attack
        self.defence_point = next_defence

    def get_hit(self,
                enemy_attack_point: BodyPoints,
                enemy_hit_power: int,
                enemy_type: CharacterType):

        if enemy_attack_point == BodyPoints.NOTHING:
            return "Opponent missed!"
        elif self.defence_point == enemy_attack_point:
            return "Hit is blocked!"
        else:
            self.hp -= enemy_hit_power * (2 if enemy_type in self.weaknesses else 1)

            if self.hp <= 0:
                self.state = CharacterState.DEFEATED
                self.hp = 0
                return "I've lost..."
            else:
                return "I'm damaged"


if __name__ == '__main__':
    p = Character('player', CharacterType.USUAL)
    print(p)
