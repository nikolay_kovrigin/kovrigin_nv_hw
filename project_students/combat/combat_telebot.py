import json

import telebot
from telebot import types

from project_students.combat.character import Character
from project_students.combat.character_body_points import BodyPoints
from project_students.combat.character_by_types import character_by_type
from project_students.combat.character_npc import CharacterNPC
from project_students.combat.character_states import CharacterState
from project_students.combat.character_types import CharacterType
from project_students.combat.character_types_weaknesses import character_weaknesses_by_type
from project_students.combat.combat_result import CombatResult

with open('../token', 'r') as token_file:
    token = token_file.read().strip()

bot = telebot.TeleBot(token)

# {id: {'username': name_str,
#       'user_character': Character_obj,
#       'npc_character': CharacterNPC_obj,
#       'wins_streak': int}}
state = {}

# {id: {'total wins' : int,
#       'longest winning streak' : int,
#       'last winning streak' : int,
#       'total losses' : int
#       'image settings' : str }}
journal = {}
journal_file = 'combat_journal.json'


def update_journal(user_id, result=None, wins_streak=None):
    print('Update journal...', end=' ')
    global journal

    if journal[user_id].get('total wins') is None:
        journal[user_id]['total wins'] = 0
        journal[user_id]['longest winning streak'] = 0
        journal[user_id]['last winning streak'] = 0
        journal[user_id]['total losses'] = 0

    if result == CombatResult.WIN:
        journal[user_id]['total wins'] += 1
        journal[user_id]['last winning streak'] = wins_streak
        if journal[user_id]['longest winning streak'] < wins_streak:
            journal[user_id]['longest winning streak'] = wins_streak
    elif result == CombatResult.LOSE:
        journal[user_id]['total losses'] += 1
    else:
        print(f'Unknown result: {result}')

    with open(journal_file, 'w') as file:
        json.dump(journal, file)

    print('completed!')


def load_journal():
    print('Load journal file...', end=' ')
    global journal
    try:
        with open(journal_file, 'r') as file:
            journal = json.load(file)
        print('success!')
    except FileNotFoundError:
        journal = {}
        print('file not found.')


@bot.message_handler(commands=["stat"])
def stat(message):
    global journal
    if journal.get(str(message.chat.id)) is None or journal[str(message.chat.id)].get('total wins') is None:
        msg = "No games have been played yet"
    else:
        msg = "Your results:"
        for res, num in journal[str(message.chat.id)].items():
            if res != 'image settings':
                msg += f"\n{res}: {num}"
    bot.send_message(message.chat.id,
                     text=msg)


@bot.message_handler(commands=['help', 'info'])
def help_command(message):
    text = f'Greetings! I know following commands:\n\n' \
           f'/start - start combat\n' \
           f'/stat - show game statistic\n' \
           f'/image -  change "graphics" settings\n' \
           f'/weaknesses - show weaknesses by type\n'
    bot.send_message(message.chat.id, text=text)


@bot.message_handler(commands=['weaknesses'])
def weaknesses(message):
    msg = ''
    for typ, weakns in character_weaknesses_by_type.items():
        msg += f"{typ.name}  :  {',  '.join([weak.name for weak in weakns])}\n"
    bot.send_message(message.chat.id, msg)


@bot.message_handler(commands=['image', 'graphics'])
def images(message):
    bot.send_message(message.chat.id,
                     'You can change the "graphics" settings.')
    image_command(message)


def image_command(message):
    graphics_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                  one_time_keyboard=True,
                                                  row_width=2).row('Low', 'High')
    bot.send_message(message.chat.id,
                     text='Choose from two options:',
                     reply_markup=graphics_keyboard)
    bot.register_next_step_handler(message, graphics_handler)


def graphics_handler(message):
    global journal
    if journal.get(str(message.chat.id)) is None:
        journal[str(message.chat.id)] = {}
        journal[str(message.chat.id)]['image settings'] = '_16.png'
    if message.text.lower() == 'low':
        journal[str(message.chat.id)]['image settings'] = '_16.png'
    elif message.text.lower() == 'high':
        journal[str(message.chat.id)]['image settings'] = '_128.png'
    else:
        image_command(message)
    bot.send_message(message.chat.id, 'Settings applied')


def yes_no_keyboard():
    return types.ReplyKeyboardMarkup(resize_keyboard=True,
                                     one_time_keyboard=True,
                                     row_width=2).row('Yes', 'No')


@bot.message_handler(commands=['start'])
def start(message):
    bot.send_message(message.chat.id,
                     text='Are you ready to start?',
                     reply_markup=yes_no_keyboard())
    bot.register_next_step_handler(message, start_handler)


def start_handler(message):
    if message.text.lower() == 'yes':
        username = '@' + message.from_user.username if message.from_user.username is not None \
            else message.from_user.first_name
        bot.send_message(message.chat.id,
                         text=f'{username} has started combat!')
        print(f'Start combat with id = {message.chat.id}')
        global state
        if state.get(message.chat.id) is None:
            print(f'Created "state" for id = {message.chat.id}')
            state[message.chat.id] = {}
            state[message.chat.id]['username'] = username
        state[message.chat.id]['wins_streak'] = 0
        state[message.chat.id]['user_character'] = None
        create_npc(message)
        ask_user_about_type(message)

    elif message.text.lower() == 'no':
        bot.send_message(message.chat.id,
                         "Ok i'll wait.")
    else:
        bot.send_message(message.chat.id,
                         'Only "yes" or "no"')
        start(message)


def create_npc(message):
    global state
    character_npc = CharacterNPC()
    state[message.chat.id]['npc_character'] = character_npc
    image_filename = character_by_type[character_npc.character_type][character_npc.name]
    bot.send_message(message.chat.id, 'Your enemy:')
    global journal
    if journal.get(str(message.chat.id)) is None:
        image = '_16.png'
        journal[str(message.chat.id)] = {}
        journal[str(message.chat.id)]['image settings'] = image
    else:
        image = journal[str(message.chat.id)]['image settings']
    with open(f'../enemy_img/{image_filename}{image}', 'rb') as file:
        bot.send_photo(message.chat.id, file, character_npc)
    print(f'Created "npc_character" for id = {message.chat.id}')


def ask_user_about_type(message):
    if state[message.chat.id].get('user_character') is None:
        current_user_type = CharacterType.USUAL
    else:
        current_user_type = state[message.chat.id]['user_character'].character_type
    other_types = [char_type.name for char_type in CharacterType if char_type.name != current_user_type.name]
    markup = types.InlineKeyboardMarkup()
    b_type_0 = types.InlineKeyboardButton(text=other_types[0], callback_data=f'type_{other_types[0]}')
    b_type_1 = types.InlineKeyboardButton(text=other_types[1], callback_data=f'type_{other_types[1]}')
    markup.add(b_type_0, b_type_1)
    b_type_2 = types.InlineKeyboardButton(text=other_types[2], callback_data=f'type_{other_types[2]}')
    b_type_3 = types.InlineKeyboardButton(text=other_types[3], callback_data=f'type_{other_types[3]}')
    markup.add(b_type_2, b_type_3)
    markup.add(types.InlineKeyboardButton(text=f'keep current: {current_user_type.name}',
                                          callback_data=f'type_{current_user_type.name}'))
    bot.send_message(message.chat.id,
                     text='You can change your type or keep the current one',
                     reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "type_" in call.data)
def user_type_handler(call):
    call_data_split = call.data.split('_')
    if len(call_data_split) < 2 or not call_data_split[1] in [char_type.name for char_type in CharacterType]:
        bot.send_message(call.message.chat.id, 'There was a problem. Restart the session!')
    else:
        user_type = call_data_split[1]
        create_user_character(call.message, user_type)
        ask_protect(call.message)


def create_user_character(message, user_type):
    global state
    user_character_type = [u_type for u_type in CharacterType if u_type.name == user_type][0]
    if state[message.chat.id].get('user_character') is not None:
        user_character = Character(name=f'{state[message.chat.id]["username"]}',
                                   character_type=user_character_type,
                                   hp=state[message.chat.id]['user_character'].hp)
    else:
        user_character = Character(name=f'{state[message.chat.id]["username"]}',
                                   character_type=user_character_type)
    state[message.chat.id]['user_character'] = user_character
    print(f'Created "user_character" for id = {message.chat.id}')
    bot.send_message(message.chat.id, 'Your character:')
    bot.send_message(message.chat.id, f'{user_character}')


def body_part_keyboard():
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                         one_time_keyboard=True,
                                         row_width=len(BodyPoints))
    keyboard.row(*[types.KeyboardButton(body_part.name) for body_part in BodyPoints])
    return keyboard


def ask_protect(message):
    bot.send_message(message.chat.id,
                     "Choose DEFENSE point:",
                     reply_markup=body_part_keyboard())
    bot.register_next_step_handler(message, reply_protect)


def reply_protect(message):
    if not check_body_points_reply(message):
        ask_protect(message)
    else:
        ask_attack(message, defend_point=message.text)


def ask_attack(message, defend_point):
    bot.send_message(message.chat.id,
                     "Choose ATTACK point:",
                     reply_markup=body_part_keyboard())
    bot.register_next_step_handler(message, reply_attack, defend_point)


def reply_attack(message, defend_point):
    if not check_body_points_reply(message):
        ask_attack(message, defend_point)
    else:
        attack_point = message.text
        global state
        user_character = state[message.chat.id]['user_character']
        npc_character = state[message.chat.id]['npc_character']
        user_character.next_step_points(next_attack=BodyPoints[attack_point],
                                        next_defence=BodyPoints[defend_point])
        npc_character.next_step_points()
        combat(message, user_character, npc_character)


def check_body_points_reply(message):
    if not BodyPoints.has_item(message.text):
        bot.send_message(message.chat.id, "You must select the option on the keyboard!")
        return False
    else:
        return True


def next_enemy(message):
    bot.send_message(message.chat.id,
                     text='Continue?',
                     reply_markup=yes_no_keyboard())
    bot.register_next_step_handler(message, continue_handler)


def continue_handler(message):
    if message.text.lower() == 'yes':
        bot.send_message(message.chat.id,
                         text=f'{state[message.chat.id]["username"]} continue combat!')
        print(f'Continue combat with id = {message.chat.id}')
        create_npc(message)
        ask_user_about_type(message)

    elif message.text.lower() == 'no':
        bot.send_message(message.chat.id,
                         "Ok, stop game.")
    else:
        bot.send_message(message.chat.id,
                         'Only "yes" or "no"')
        next_enemy(message)


def combat(message, user_character: Character, npc_character: CharacterNPC):
    message_npc = npc_character.get_hit(enemy_attack_point=user_character.attack_point,
                                        enemy_hit_power=user_character.hit_power,
                                        enemy_type=user_character.character_type)
    bot.send_message(message.chat.id, f"Enemy: '{message_npc}'\nHP: {npc_character.hp}")

    message_user = user_character.get_hit(enemy_attack_point=npc_character.attack_point,
                                          enemy_hit_power=npc_character.hit_power,
                                          enemy_type=npc_character.character_type)
    bot.send_message(message.chat.id, f"You: '{message_user}'\nHP: {user_character.hp}")

    global state
    if user_character.state == CharacterState.READY and npc_character.state == CharacterState.READY:
        ask_protect(message)
    elif user_character.state == CharacterState.READY and npc_character.state == CharacterState.DEFEATED:
        # bot.send_message(message.chat.id, 'Win')
        bot.send_sticker(message.chat.id, 'CAACAgIAAxkBAAIG7GBM4w2GH5MiChJz4IQf6USsqOwzAAKRCwACdbs5SpjT-_CrpYokHgQ')
        state[message.chat.id]['wins_streak'] += 1
        update_journal(str(message.chat.id), CombatResult.WIN, state[message.chat.id]['wins_streak'])
        next_enemy(message)
    else:
        # bot.send_message(message.chat.id, 'Lose')
        bot.send_sticker(message.chat.id, 'CAACAgIAAxkBAAIG7WBM43pb470-NeycQR8S9MmpzLI3AAJnDAACGAg4Sr8jNh_saECEHgQ')
        update_journal(str(message.chat.id), CombatResult.LOSE)
        del state[message.chat.id]['user_character']


if __name__ == '__main__':
    load_journal()
    print('Starting the bot...')
    bot.polling(none_stop=True, interval=0)
    print('The bot has stopped!')
