from enum import Enum, auto


class CharacterState(Enum):
    READY = auto()
    DEFEATED = auto()
