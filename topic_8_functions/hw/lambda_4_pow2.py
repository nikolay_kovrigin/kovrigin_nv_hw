def lambda_4(n):
    """
    lambda возводит в квадрат переданный аргумент возвращает результат
    """
    print(str(n), '^ 2 = ', (lambda x: x ** 2)(n))


if __name__ == '__main__':
    lambda_4(0)
    lambda_4(2)
    lambda_4(3)
    lambda_4(4)
    lambda_4(5)
