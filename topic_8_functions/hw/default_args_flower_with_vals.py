def flower_with_vals(name="ромашка", color="белый", price=10.25):
    """
    Функция flower_with_vals.

    Принимает 3 аргумента:
    цветок (по умолчанию "ромашка"),
    цвет (по умолчанию "белый"),
    цена (по умолчанию 10.25).

    Функция flower_with_default_vals выводит строку в формате
    "Цветок: <цветок> | Цвет: <цвет> | Цена: <цена>".

    При этом в функции flower_with_default_vals есть проверка на то, что цветок и цвет - это строки

    (* Подсказка: type(x) == str или isinstance(s, str)), а также цена - это число больше 0, но меньше 10000.
    В функции main вызвать функцию flower_with_default_vals различными способами
    (перебрать варианты: цветок, цвет, цена, цветок цвет, цветок цена, цвет цена, цветок цвет цена).

    (* Использовать именованные аргументы).
    """
    if not isinstance(name, str) or not isinstance(color, str):
        print('Flower name and flower color must be string!')
        return
    if not isinstance(price, (int, float)) or (price < 0 or price > 10000):
        print('Flower price must be a NUMBER and be from 0 to 10000')
        return
    print(f'Цветок: {name} | Цвет: {color} | Цена: {price}')


if __name__ == '__main__':
    # # checks
    # flower_with_vals(name=1)
    # flower_with_vals(color=2)
    # flower_with_vals(price='3')
    # flower_with_vals(price=-1)
    # flower_with_vals(price=10001)

    flower_with_vals(name='тюльпан')
    flower_with_vals(color='желтый')
    flower_with_vals(price=9.90)
    flower_with_vals(name='роза', color='красная')
    flower_with_vals(name='орхидея', price=9900)
    flower_with_vals(color='черная', price=3)
    flower_with_vals(name='ландыш', color='серебристый', price=500)
