def lambda_2(a, b, c):
    """
    lambda суммирует аргументы a, b и c и выводит результат
    """
    print(str(a), ' + ', str(b), ' + ', str(c), ' = ', (lambda x, y, z: x + y + z)(a, b, c))


if __name__ == '__main__':
    lambda_2(1, 1, 1)
    lambda_2(0, 0, 0)
    lambda_2(0, 2, 3)
    lambda_2(5, 8, 7)
