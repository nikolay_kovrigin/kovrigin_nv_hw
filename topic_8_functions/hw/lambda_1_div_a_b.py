def lambda_1(a, b):
    """
    lambda делит аргумент a на аргумент b и выводит результат
    """
    print('a = ', str(a), '\t| b = ', str(b), '\t| a/b = ', (lambda x, y: x / y)(a, b))

if __name__ == '__main__':
    lambda_1(18, 2)
    lambda_1(10, 5)
    lambda_1(256, 16)
    lambda_1(500, 200)
