def print_soap_price(name, *prices):
    """
    Функция print_soap_price.

    Принимает 2 аргумента: название мыла (строка) и неопределенное количество цен (*args).

    Функция print_soap_price выводит вначале название мыла, а потом все цены на него.

    В функции main вызывается функция print_soap_price и передается название мяла и произвольное количество цен.

    Пример: print_soap_price(‘Dove’, 10, 50) или print_soap_price(‘Мылко’, 456, 876, 555).
    """
    print(f'Name: {name}\n'
          f'Prices: {"; ".join([str(price) for price in list(prices)])}.\n{"-"*20}')


if __name__ == '__main__':
    print_soap_price('Dove', 10, 50)
    print_soap_price('Мылко', 456, 876, 555)
    print_soap_price('SuperSoap', 1999, 2999, 4999, 19990)