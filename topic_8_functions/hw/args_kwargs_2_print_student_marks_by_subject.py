def print_student_marks_by_subject(name, **perf):
    """
    Функция print_student_marks_by_subject.

    Принимает 2 аргумента: имя студента и именованные аргументы с оценками по различным предметам (**kwargs).

    Выводит на экран имя студента, а затем предмет и оценку (может быть несколько, если курс состоял из нескольких частей).

    Пример вызова функции print_student_marks_by_subject("Вася", math=5, biology=(3, 4), magic=(4, 5, 5)).
    """
    print(f'{"-" * 20}\n'
          f'Student: {name}')
    for subj, marks in perf.items():
        print(f'Subject: {subj} | '
              f'Marks: {marks if isinstance(marks, int) else ", ".join([str(mark) for mark in marks])}')


if __name__ == '__main__':
    print_student_marks_by_subject("Вася", math=5, biology=(3, 4), magic=(4, 5, 5))
    print_student_marks_by_subject("Петя", english=(2, 3), biology=(5, 5), history=(5, 4, 5))
    print_student_marks_by_subject("Вова", russian=(5, 4), math=3, geography=5)
