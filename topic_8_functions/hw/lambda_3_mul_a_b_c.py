def lambda_3(a, b, c):
    """
    lambda перемножает аргументы a, b и c и выводит результат
    """
    print(str(a), ' * ', str(b), ' * ', str(c), ' = ', (lambda x, y, z: x * y * z)(a, b, c))


if __name__ == '__main__':
    lambda_3(1, 1, 1)
    lambda_3(0, 2, 3)
    lambda_3(2, 2, 2)
    lambda_3(5, 5, 1)
