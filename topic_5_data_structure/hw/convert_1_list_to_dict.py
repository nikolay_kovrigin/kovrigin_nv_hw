def list_to_dict(my_list, val):
    """
    Функция list_to_dict.

    Принимает 2 аргумента: список и значение для поиска.

    Возвращает словарь (из входного списка), в котором
    ключ = значение из списка,
    значение = (
    индекс (последний, если есть несколько одинаковых значений) этого элемента в списке,
    равно ли значение из списка искомому значению (True | False),
    количество элементов в словаре.
    )

    Если список пуст, то возвращать пустой словарь.

    Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

    ВНИМАНИЕ: количество элементов в словаре не всегда равно количеству элементов в списке!

    ВНИМАНИЕ: при повторяющихся ключах в dict записывается значение последнего добавленного ключа.

    ВНИМАНИЕ: нумерация индексов начинается с 0!
    """
    my_dict = {}

    if not isinstance(my_list, list):
        return 'First arg must be list!'
    if not list:
        return my_dict

    # # My original
    # i = 0
    # for key in my_list:
    #     my_dict[key] = (i, key == val, len(set(my_list)))
    #     i += 1
    # return my_dict

    # # alternative
    # for index, key in enumerate(my_list):
    #     my_dict[key] = (index, key == val, len(set(my_list)))
    # return my_dict

    # alternative 2
    return {key: (index, key == val, len(set(my_list))) for index, key in enumerate(my_list)}
