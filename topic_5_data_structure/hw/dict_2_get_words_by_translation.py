def get_words_by_translation(ru_eng_dict, word):
    """

    :param ru_eng_dict:
    :param word:
    :return:
    """
    """
    Функция get_words_by_translation.

    Принимает 2 аргумента:
    ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
    слово для поиска в словаре (eng).

    Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
    если нет, то ‘Can`t find English word: {word}’.

    Если вместо словаря передано что-то другое, то возвращать строку 'Dictionary must be dict!'.
    Если вместо строки для поиска передано что-то другое, то возвращать строку 'Word must be str!'.

    Если словарь пустой, то возвращать строку 'Dictionary is empty!'.
    Если строка для поиска пустая, то возвращать строку 'Word is empty!'.
    """

    if type(ru_eng_dict) != dict:
        return 'Dictionary must be dict!'
    if type(word) != str:
        return 'Word must be str!'
    if len(ru_eng_dict) == 0:
        return 'Dictionary is empty!'
    if len(word) == 0:
        return 'Word is empty!'

    # My origin
    translate = []
    for ru_word in ru_eng_dict.keys():
        for eng_word in ru_eng_dict[ru_word]:
            if eng_word == word:
                translate.append(ru_word)
                break

    return translate if translate else f"Can`t find English word: {word}"

    # # alternative
    # translate = []
    # for ru_word, eng_word in ru_eng_dict.items():
    #     if word in eng_word:
    #         translate.append(ru_word)
    # return translate if translate else f"Can`t find English word: {word}"
