def dict_to_list(my_dict):
    """
    Функция dict_to_list.

    Принимает 1 аргумент: словарь.

    Возвращает кортеж: (
    список ключей,
    список значений,
    количество уникальных элементов в списке ключей,
    количество уникальных элементов в списке значений
    ).

    Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.
    """
    if not isinstance(my_dict, dict):
        return 'Must be dict!'

    keys = list(my_dict.keys())
    values = list(my_dict.values())

    return keys, values, len(keys), len(set(values))
