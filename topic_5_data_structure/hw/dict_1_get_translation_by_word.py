def get_translation_by_word(ru_eng_dict, word):
    """
    Функция get_translation_by_word.

    Принимает 2 аргумента:
    ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
    слово для поиска в словаре (ru).

    Возвращает все варианты переводов (list), если такое слово есть в словаре,
    если нет, то ‘Can`t find Russian word: {word}’.

    Если вместо словаря передано что-то другое, то возвращать строку 'Dictionary must be dict!'.
    Если вместо строки для поиска передано что-то другое, то возвращать строку 'Word must be str!'.

    Если словарь пустой, то возвращать строку 'Dictionary is empty!'.
    Если строка для поиска пустая, то возвращать строку 'Word is empty!'.
    """
    if type(ru_eng_dict) != dict:
        return 'Dictionary must be dict!'
    if type(word) != str:
        return 'Word must be str!'
    if len(ru_eng_dict) == 0:
        return 'Dictionary is empty!'
    if len(word) == 0:
        return 'Word is empty!'

    # My original
    for ru_word in ru_eng_dict.keys():
        if ru_word == word:
            return ru_eng_dict[word]
    else:
        return f"Can`t find Russian word: {word}"

    # # alternative 1
    # if word in ru_eng_dict:
    #     return ru_eng_dict[word]
    # else:
    #     return f"Can`t find Russian word: {word}"

    # # ideal
    # return ru_eng_dict.get(word, f"Can`t find Russian word: {word}")