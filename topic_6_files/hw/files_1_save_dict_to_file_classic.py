def save_dict_to_file_classic(path, data):
    """
    Функция save_dict_to_file_classic.

    Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

    Сохраняет список в файл. Проверить, что записалось в файл.
    """
    with open(path, 'w') as file:
        file.write(str(data))
    with open(path, 'r') as file:
        check = file.read()

    print(check == str(data))


if __name__ == '__main__':
    save_dict_to_file_classic('file_classic.txt', {'a': 1, 'b': 2, 'c': 3})
