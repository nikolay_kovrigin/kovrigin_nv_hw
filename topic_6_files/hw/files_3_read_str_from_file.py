def read_str_from_file(path):
    """
    Функция read_str_from_file.
    
    Принимает 1 аргумент: строка (название файла или полный путь к файлу).
    
    Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
    """
    with open(path, 'r') as f:
        print(f.read())


if __name__ == '__main__':
    file_name = 'hw_file_3.txt'
    data = read_str_from_file.__doc__
    with open(file_name, 'w') as file:
        file.write(str(data))

    read_str_from_file(file_name)
