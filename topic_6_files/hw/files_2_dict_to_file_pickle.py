import pickle


def save_dict_to_file_pickle(path, data):
    """
    Функция save_dict_to_file_pickle.

    Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

    Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
    """
    with open(path, 'wb') as file:
        pickle.dump(data, file)
    with open(path, 'rb') as file:
        check = pickle.load(file)

    print(check == data)


if __name__ == '__main__':
    save_dict_to_file_pickle('file_pickle.pkl', {'a': 1, 'b': 2, 'c': 3})
