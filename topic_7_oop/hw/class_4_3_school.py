from topic_7_oop.hw.class_4_1_pupil import Pupil
from topic_7_oop.hw.class_4_2_worker import Worker


class School:
    """
    Класс School.

    Поля:
    список людей в школе (общий list для Pupil и Worker): people,
    номер школы: number.

    Методы:
    get_avg_mark: вернуть средний балл всех учеников школы
    get_avg_salary: вернуть среднюю зп работников школы
    get_worker_count: вернуть сколько всего работников в школе
    get_pupil_count: вернуть сколько всего учеников в школе
    get_pupil_names: вернуть все имена учеников (с повторами, если есть)
    get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
    get_max_pupil_age: вернуть возраст самого старшего ученика
    get_min_worker_salary: вернуть самую маленькую зп работника
    get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
    (список из одного или нескольких элементов)
    """
    def __init__(self, people, number):
        self.people = people
        self.number = number

    def get_avg_mark(self):
        marks = [hum.get_avg_mark() for hum in self.people if isinstance(hum, Pupil)]
        return sum(marks) / len(marks)

    def get_avg_salary(self):
        salary = [hum.salary for hum in self.people if isinstance(hum, Worker)]
        return sum(salary) / len(salary)

    def get_worker_count(self):
        return len([hum.name for hum in self.people if isinstance(hum, Worker)])

    def get_pupil_count(self):
        return len([hum.name for hum in self.people if isinstance(hum, Pupil)])

    def get_pupil_names(self):
        return [hum.name for hum in self.people if isinstance(hum, Pupil)]

    def get_unique_worker_positions(self):
        return set([hum.position for hum in self.people if isinstance(hum, Worker)])

    def get_max_pupil_age(self):
        return max([hum.age for hum in self.people if isinstance(hum, Pupil)])

    def get_min_worker_salary(self):
        return min([hum.salary for hum in self.people if isinstance(hum, Worker)])

    def get_min_salary_worker_names(self):
        return [hum.name for hum in self.people if type(hum) == Worker and hum.salary == self.get_min_worker_salary()]
