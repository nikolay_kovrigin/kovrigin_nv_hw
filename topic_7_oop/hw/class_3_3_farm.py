from topic_7_oop.hw.class_3_1_chicken import Chicken
from topic_7_oop.hw.class_3_2_goat import Goat


class Farm:
    """
    Класс Farm.

    Поля:
    животные (list из произвольного количества Goat и Chicken): animals
    (вначале список пуст, потом добавялем животных методами append и extend самостоятельно),
    наименование фермы: name,
    имя владельца фермы: owner.

    Методы:
    get_goat_count: вернуть количество коз на ферме (*Подсказка isinstance или type == Goat),
    get_chicken_count: вернуть количество куриц на ферме,
    get_animals_count: вернуть количество животных на ферме,
    get_milk_count: вернуть сколько молока можно получить в день,
    get_eggs_count: вернуть сколько яиц можно получить в день.
    """
    def __init__(self, name, owner):
        self.name = name
        self.owner = owner
        self.zoo_animals = list()

    def get_goat_count(self):
        return len([animal.name for animal in self.zoo_animals if isinstance(animal, Goat)])

    def get_chicken_count(self):
        return len([animal.name for animal in self.zoo_animals if isinstance(animal, Chicken)])

    def get_animals_count(self):
        return len([animal.name for animal in self.zoo_animals])

    def get_milk_count(self):
        return sum([animal.milk_per_day for animal in self.zoo_animals if type(animal) == Goat])

    def get_eggs_count(self):
        return sum([animal.eggs_per_day for animal in self.zoo_animals if type(animal) == Chicken])
