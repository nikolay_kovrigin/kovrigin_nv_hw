"""
Ввод данных с клавиатуры:
- Имя
- Фамилия
- Возраст

Вывод данных в формате:
"Вас зовут <Имя> <Фамилия>! Ваш возраст равен <Возраст>."
"""

name = input('Input name: ')
surname = input('Input surname: ')
age = input('Input age: ')
print(f'Вас зовут {name} {surname}! Ваш возраст равен {age}.')
